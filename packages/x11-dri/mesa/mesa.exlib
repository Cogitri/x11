# Copyright 2008 Alexander Færøy <eroyf@eroyf.org>
# Copyright 2008-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mesa-7.0.3.ebuild' which is:
#    Copyright 1999-2008 Gentoo Foundation

if ever is_scm ; then
    SCM_REPOSITORY="https://anongit.freedesktop.org/git/${PN}/${PN}.git"
    require scm-git
else
    MY_PV=${PV%%-rc*}
    DOWNLOADS="
        https://mesa.freedesktop.org/archive/${MY_PV}/${PNV}.tar.xz
        https://mesa.freedesktop.org/archive/${PNV}.tar.xz
    "
    UPSTREAM_RELEASE_NOTES="https://www.mesa3d.org/relnotes/${PV}.html"
fi

require alternatives toolchain-funcs
require flag-o-matic
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

export_exlib_phases src_configure src_install pkg_postinst

OPENGL_DIR="xorg-x11"

SUMMARY="Mesa's OpenGL library"
HOMEPAGE="https://www.mesa3d.org"

# Licences for parts we don't install:
# GLUT: Mark Kilgard's copyright
LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    llvm [[ description = [ Using LLVM as the shader backend for Gallium ] ]]
    llvm-static [[ description = [ Link to LLVM statically ] requires = [ llvm ] ]]
    opencl [[ description = [ Enable the OpenCL Gallium state tracker ] requires = [ llvm ] ]]
    texture-float [[ description = [ Enable floating-point textures and renderbuffers. SGI claims to hold patents on this code. ] ]]
    va [[ description = [ Enable the VA-API state tracker ] ]]
    wayland
    X [[ description = [ Enable GLX ] presumed = true ]]
    xa [[ description = [ Enable the X Acceleration API (xatracker) ] ]]
    xvmc [[ description = [ Enable support for XvMC hardware accelerated video decoding ] ]]
    (
        platform:
            amd64
            x86
        video_drivers:
            (
                i915 [[ description = [ Gallium driver for i915 based Intel GPUs ] ]]
                intel [[ description = [ Driver for i915, i965 and newer ] ]]
            ) [[ number-selected = at-most-one ]]
            gallium-swrast [[ description = [ Gallium based software driver ] ]]
            (
                nouveau [[ description = [ (Gallium) Generally for newer Nvidia cards ] ]]
                nouveau-legacy [[ description = [ Generally for older Nvidia cards ] ]]
            ) [[ number-selected = at-most-one ]]
            (
                r300 [[ description = [ (Gallium) Driver for Radeon cards from 9500 (r300) to X1950 (r500) ] ]]
                radeon [[ description = [ (Gallium) Driver for the Radeon HD-2000 (r600) and newer ] ]]
            ) [[ requires = [ llvm video_drivers: gallium-swrast ] ]]
            radeon-legacy [[ description = [ Classic driver for cards older than the Radeon 9500 (r300) ] ]]
            virtio-gpu [[ description = [ Gallium driver for guest systems on hypervisors with VirtIO-GPU ] ]]
            vmware [[ description = [ Gallium driver for VMware's SVGA virtual GPU ] ]]
    )
    llvm? (
        ( video_drivers: gallium-swrast i915 nouveau r300 radeon ) [[ number-selected = at-least-one ]]
    )
    opencl? (
        ( video_drivers: nouveau radeon ) [[ number-selected = at-least-one ]]
    )
    vdpau? ( ( video_drivers: nouveau radeon ) [[ number-selected = at-least-one ]] )
    xa? ( ( video_drivers: i915 nouveau vmware ) [[ number-selected = at-least-one ]] )
    xvmc? ( ( video_drivers: nouveau radeon ) [[ number-selected = at-least-one ]] )
"

if ever at_least 17.1.3; then
    MYOPTIONS+="
        vdpau [[ description = [ Enable support for VDPAU hardware accelerated video decoding ] requires = [ X ] ]]
        va? (
            ( X wayland ) [[ number-selected = at-least-one ]]
        )
    "
else
    MYOPTIONS+="
        vdpau [[ description = [ Enable support for VDPAU hardware accelerated video decoding ] ]]
    "
fi

if ever at_least 13.0.0; then
    MYOPTIONS+="
        sensors [[ description = [ Gallium extra HUD and sensors support ] ]]
        sensors? ( ( video_drivers: nouveau radeon ) [[ number-selected = at-least-one ]] )
    "
else
    MYOPTIONS+="
        ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    "
fi

if ever at_least 12.0.0-rc1; then
    MYOPTIONS+="
        gl-vendor [[
            description = [ Install a glvnd (gl vendor) file to allow for parallel install of multiple GL implementations ]
        ]]
        valgrind [[ description = [ Support valgrind intrinsics to suppress false warnings ] ]]
    "
fi

if ! ever at_least 17.1.3; then
    MYOPTIONS+="
        ( providers: gcrypt libressl nettle openssl ) [[ number-selected = exactly-one ]]
    "
fi

if ever at_least 17.1.3; then
    DRI2PROTO_REQUIRED='[>=2.8]'
else
    DRI2PROTO_REQUIRED='[>=2.6]'
fi

GLPROTO_REQUIRED='[>=1.4.14]'
XCBDRI2_REQUIRED='[>=1.8]'

# these high versions are not required for every package but as long as we don't need to deal with different versions use the latest
if ever at_least 17.3.0; then
    LIBDRM_REQUIRED='[>=2.4.85]'
elif ever at_least 17.2.0; then
    LIBDRM_REQUIRED='[>=2.4.82]'
elif ever at_least 17.1.3; then
    LIBDRM_REQUIRED='[>=2.4.80]'
else
    LIBDRM_REQUIRED='[>=2.4.69]'
fi


DEPENDENCIES="
    build:
        dev-lang/python:2.7
        dev-python/Mako[>=0.3.4][python_abis:2.7]
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config[>=0.9.0]
        !x11-dri/eclectic-opengl
        llvm? (
             !llvm-static? ( dev-lang/llvm[>=3.7][-static] )
             llvm-static? ( dev-lang/llvm[>=3.7][static] )
        )
        opencl? (
            dev-lang/clang[>=3.4]
        )
        video_drivers:radeon? ( dev-lang/llvm[>=3.6.0] )
        X? (
            x11-proto/dri2proto${DRI2PROTO_REQUIRED}
            x11-proto/glproto${GLPROTO_REQUIRED}
        )
    build+run:
        dev-libs/expat
        x11-dri/libdrm${LIBDRM_REQUIRED}
        opencl? ( dev-libs/libclc )
        va? (
            x11-libs/libX11
            x11-libs/libva[>=1.6.0]
            x11-libs/libxcb${XCBDRI2_REQUIRED}
        )
        vdpau? (
            x11-libs/libX11
            x11-libs/libvdpau[>=1.1]
            x11-libs/libxcb${XCBDRI2_REQUIRED}
        )
        video_drivers:i915? ( x11-dri/libdrm[video_drivers:intel(+)] )
        video_drivers:intel? ( x11-dri/libdrm[video_drivers:intel(+)] )
        video_drivers:nouveau? ( x11-dri/libdrm[video_drivers:nouveau(-)] )
        video_drivers:nouveau-legacy? ( x11-dri/libdrm[video_drivers:nouveau(-)] )
        video_drivers:r300? ( x11-dri/libdrm[video_drivers:radeon(-)] )
        video_drivers:radeon? (
            dev-util/elfutils
            x11-dri/libdrm[video_drivers:radeon(-)]
        )
        video_drivers:radeon-legacy? ( x11-dri/libdrm[video_drivers:radeon(-)] )
        video_drivers:vmware? ( x11-dri/libdrm[video_drivers:vmware(-)] )
        wayland? ( sys-libs/wayland[>=1.2.0] )
        X? (
            x11-libs/libICE
            x11-libs/libX11[xcb(+)]
            x11-libs/libXau
            x11-libs/libxcb[>=1.9.3]
            x11-libs/libXdamage[>=1.1.1]
            x11-libs/libXdmcp
            x11-libs/libXext
            x11-libs/libXfixes
            x11-libs/libxshmfence[>=1.1]
            x11-libs/libXxf86vm
        )
        xvmc? (
            x11-libs/libX11
            x11-libs/libXvMC[>=1.0.6]
            x11-libs/libxcb${XCBDRI2_REQUIRED}
        )
    run:
        llvm? (
            !llvm-static? ( dev-lang/llvm[>=3.7][-static] )
        )
        opencl? (
            dev-libs/ocl-icd
        )
    suggestion:
        x11-dri/mesa-demos
"

if ever at_least 13.0.0; then
    DEPENDENCIES+="
        build:
            dev-python/Mako[>=0.8.0][python_abis:2.7]
        build+run:
            sensors? ( sys-apps/lm_sensors[>=4.0.0] )
    "
else
    DEPENDENCIES+="
        build:
            providers:eudev? ( sys-apps/eudev )
            providers:systemd? ( sys-apps/systemd[>=151] )
    "
fi

if ever at_least 12.0.0-rc1; then
    DEPENDENCIES+="
        build:
            gl-vendor? ( dev-libs/libglvnd )
            valgrind? ( dev-util/valgrind )
    "
fi

if ! ever at_least 17.1.3; then
    DEPENDENCIES+="
        build+run:
            X? (
                x11-proto/dri3proto
                x11-proto/presentproto
            )
            providers:gcrypt? ( dev-libs/libgcrypt )
            providers:libressl? ( dev-libs/libressl:= )
            providers:nettle? ( dev-libs/nettle )
            providers:openssl? ( dev-libs/openssl )
    "
fi

if ever at_least 17.1.3; then
    DEPENDENCIES+="
        build:
            dev-libs/libpthread-stubs[>=0.4]
            gl-vendor? ( dev-libs/libglvnd[>=0.2.0] )
            video_drivers:radeon? ( dev-lang/llvm[>=3.9.0] )
        build+run:
            sys-libs/zlib[>=1.2.3]
            wayland? ( sys-libs/wayland[>=1.11] )
    "
fi

if ever at_least 17.2.0; then
    DEPENDENCIES+="
        build:
            dev-libs/libpthread-stubs[>=0.4]
            gl-vendor? ( dev-libs/libglvnd[>=0.2.0] )
            video_drivers:radeon? ( dev-lang/llvm[>=3.9.0] )
        build+run:
            sys-libs/zlib[>=1.2.3]
            wayland? (
                sys-libs/wayland[>=1.11]
                sys-libs/wayland-protocols[>=1.8.0]
            )
    "
fi

if ever at_least 17.3.0; then
    DEPENDENCIES+="
        build:
            opencl? ( dev-lang/llvm[>=3.9.0] )
        build+run:
            !media-libs/libtxc_dxtn [[
                description = [ mesa now bundles libtxc_dxtn ]
                resolution = uninstall-blocked-after
            ]]
    "
fi

DEFAULT_SRC_TEST_PARAMS=( PTHREAD_LIBS="-lpthread" )

AT_M4DIR=( m4 )

mesa_src_configure() {
    DRI_DRIVERS=( )
    EGL_PLATFORMS=( )
    GALLIUM_DRIVERS_NOSW=( )
    GALLIUM_DRIVERS=( )
    VULKAN_DRIVERS=( )

    option !video_drivers:gallium-swrast    && DRI_DRIVERS+=( swrast )
    option video_drivers:intel              && DRI_DRIVERS+=( i915 i965 )
    option video_drivers:nouveau-legacy     && DRI_DRIVERS+=( nouveau )
    option video_drivers:radeon-legacy      && DRI_DRIVERS+=( radeon r200 )

    # TODO: noop, swr, vc4, vc5, etnaviv, imx, pl111
    option video_drivers:i915               && GALLIUM_DRIVERS_NOSW+=( i915 )
    option video_drivers:nouveau            && GALLIUM_DRIVERS_NOSW+=( nouveau )
    option video_drivers:r300               && GALLIUM_DRIVERS_NOSW+=( r300 )
    option video_drivers:radeon             && GALLIUM_DRIVERS_NOSW+=( r600 radeonsi )
    option video_drivers:virtio-gpu         && GALLIUM_DRIVERS_NOSW+=( virgl )
    option video_drivers:vmware             && GALLIUM_DRIVERS_NOSW+=( svga )

    # TODO: gallium-swr
    GALLIUM_DRIVERS=( "${GALLIUM_DRIVERS_NOSW[@]}" )
    option video_drivers:gallium-swrast     && GALLIUM_DRIVERS+=( swrast )

    option X                                && EGL_PLATFORMS+=( x11 )
    option wayland                          && EGL_PLATFORMS+=( wayland )
                                               EGL_PLATFORMS+=( drm surfaceless )

    option video_drivers:intel              && VULKAN_DRIVERS+=( intel )
    option video_drivers:radeon             && VULKAN_DRIVERS+=( radeon )

    local MESA_SRC_CONFIGURE_PARAMS

    # NOTE(compnerd) disable the assembly dispatching.  The code is absolutely horrid, fragile, and
    # is disabled by other distributions as well (including Fedora and RHEL).  It is also broken
    # with LTO, and is not intended to be fixed.  https://bugs.freedesktop.org/show_bug.cgi?id=55080
    MESA_SRC_CONFIGURE_PARAMS+=(
        $(option llvm && echo --with-llvm-prefix=/usr/$(exhost --target))
        $($(option platform:x86 || option platform:amd64) && echo --disable-asm)
        --enable-osmesa # off-screen mesa
    )

    if [[ ${#DRI_DRIVERS[@]} -gt 0 ]]; then
        MESA_SRC_CONFIGURE_PARAMS+=( --with-dri-drivers=$(IFS=, ; echo "${DRI_DRIVERS[*]}") )
    else
        MESA_SRC_CONFIGURE_PARAMS+=( --without-dri-drivers )
    fi

    if [[ ${#GALLIUM_DRIVERS[*]} -gt 0 ]] ; then
        MESA_SRC_CONFIGURE_PARAMS+=( --with-gallium-drivers=$(IFS=, ; echo "${GALLIUM_DRIVERS[*]}") )
    else
        MESA_SRC_CONFIGURE_PARAMS+=( --without-gallium-drivers )
    fi

    if [[ ${#GALLIUM_DRIVERS_NOSW[*]} -gt 0 ]] ; then
        MESA_SRC_CONFIGURE_PARAMS+=( $(option video_drivers:gallium-swrast && echo "--enable-nine") )
    fi

    if ever at_least 12.0.0-rc1; then
        if [[ ${#VULKAN_DRIVERS[@]} -gt 0 ]]; then
            MESA_SRC_CONFIGURE_PARAMS+=( --with-vulkan-drivers=$(IFS=, ; echo "${VULKAN_DRIVERS[*]}") )
        else
            MESA_SRC_CONFIGURE_PARAMS+=( --without-vulkan-drivers )
        fi
        MESA_SRC_CONFIGURE_PARAMS+=( $(option_enable gl-vendor libglvnd) )
    fi

    # TODO gallium_tests
    # TODO state trackers: OpenMAX(omx), requires additional new dependency libomxil-bellagio

    MESA_SRC_CONFIGURE_PARAMS+=(
          --with-pic
          --enable-dri
          --enable-egl
          --enable-gbm
          --enable-gles1
          --enable-gles2
          --enable-shared-glapi
          --disable-glx-read-only-text
          $(option_enable !llvm-static llvm-shared-libs)
          $(option_enable opencl)
          $(option_enable opencl opencl-icd)
          $(option_enable texture-float)
          $(option_enable va)
          $(option_enable vdpau)
          $(option_enable X dri3)
          $(option_enable X glx)
          $(option_enable X glx-tls)
          $(option_enable xa)
          $(option_enable xvmc)
    )

    if ever at_least 17.3.0; then
        MESA_SRC_CONFIGURE_PARAMS+=(
            --disable-omx-bellagio
        )
    else
        MESA_SRC_CONFIGURE_PARAMS+=(
            --disable-omx
        )
    fi

    if ever at_least 12.0.0-rc1; then
        MESA_SRC_CONFIGURE_PARAMS+=( $(option_enable valgrind) )
    fi

    if ! ever at_least 12.0.2; then
        MESA_SRC_CONFIGURE_PARAMS+=(
            $(option_enable video_drivers:radeon r600-llvm-compiler)
        )
    fi

    if ever at_least 13.0.0; then
        MESA_SRC_CONFIGURE_PARAMS+=(
            $(option_enable sensors lmsensors)
            $(option_enable sensors gallium_extra_hud)
        )
    else
        MESA_SRC_CONFIGURE_PARASM+=(
            --disable-sysfs
        )
    fi

    if ever at_least 17.1.3; then
        MESA_SRC_CONFIGURE_PARAMS+=(
            --disable-libunwind
            --with-platforms=$(IFS=, ; echo "${EGL_PLATFORMS[*]}")
            --with-wayland-scanner-path=/usr/$(exhost --build)/bin/wayland-scanner
            $(option_enable llvm)
        )
    else
        MESA_SRC_CONFIGURE_PARAMS+=(
            --enable-shader-cache
            --with-egl-platforms=$(IFS=, ; echo "${EGL_PLATFORMS[*]}")
            $(option_enable llvm gallium-llvm)
        )
        if option providers:libressl || option providers:openssl; then
            MESA_SRC_CONFIGURE_PARAMS+=(
                --with-sha1=libcrypto
            )
        elif option providers:gcrypt; then
            MESA_SRC_CONFIGURE_PARAMS+=(
                --with-sha1=libgcrypt
            )
        elif option providers:nettle; then
            MESA_SRC_CONFIGURE_PARAMS+=(
                --with-sha1=libnettle
            )
        fi
    fi

    # Fix using the 32bit python while it isn't needed.
    # TODO: Something with python.exlib to require the neccessary modules to be built for the
    #       selected python.
    #PYTHON2=/usr/bin/python2 \
    # TODO: check if something similar to the above might be necessary when cross-compiling
    # mesa. Obviously the path doesn't work on cross. (heirecka)
    econf "${MESA_SRC_CONFIGURE_PARAMS[@]}"
}

mesa_src_install() {
    dodir /usr

    local host=$(exhost --target)
    local X_alternatives=()

    default

    # alternatives
    local libs=(libEGL libGLESv1_CM libGLESv2)
    option X && libs+=(libGL)

    edo mkdir "${IMAGE}"/usr/${host}/lib/opengl/

    local lib path_old path_new soname
    for lib in "${libs[@]}"; do
        path_new=/usr/${host}/lib/opengl/${lib}-xorg.so
        path_old=$(readlink -f "${IMAGE}"/usr/${host}/lib/${lib}.so)

        edo mv \
            "${path_old}" \
            "${IMAGE}"/${path_new}

        local objdump=$(exhost --tool-prefix)objdump
        soname=$(edo ${objdump} -p "${IMAGE}"/${path_new} | sed -n 's/^ *SONAME *//p')

        # clean up the leftover symlinks
        edo rm "${IMAGE}"/usr/${host}/lib/{${lib}.so,${soname}}

        X_alternatives+=(
            /usr/${host}/lib/${lib}.so ${path_new}
            /usr/${host}/lib/${soname} ${path_new}
        )
    done

    alternatives_for opengl X.org 10 ${X_alternatives[@]}

    edo rm -rf "${IMAGE}"/usr/${host}/include/GL/{glew,glxew,wglew}.h
}

mesa_pkg_postinst() {
    # Clean up after old eclectic-opengl module
    if [[ -f ${ROOT}/etc/env.d/03opengl ]] ; then
        rm "${ROOT}/etc/env.d/03opengl" || ewarn "Remove /etc/env.d/03opengl manually"
    fi

    alternatives_pkg_postinst
}

