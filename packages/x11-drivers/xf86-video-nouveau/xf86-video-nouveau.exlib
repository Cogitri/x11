# Copyright 2008-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg

if ever is_scm ; then
    SCM_REPOSITORY="https://anongit.freedesktop.org/git/nouveau/${PN}.git"
    require scm-git
else
    DOWNLOADS="https://xorg.freedesktop.org/archive/individual/driver/${PNV}.tar.bz2"
fi

SUMMARY="Open Source 2D/3D acceleration for NVIDIA cards"
HOMEPAGE="https://nouveau.freedesktop.org"

UPSTREAM_CHANGELOG="https://cgit.freedesktop.org/nouveau/${PN}/log/"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/wiki/FAQ [[ note = FAQ ]]"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        x11-proto/dri2proto[>=2.6]
        x11-proto/fontsproto
        x11-proto/randrproto
        x11-proto/renderproto
        x11-proto/videoproto
        x11-proto/xextproto[>=7.1]
        x11-proto/xproto
    build+run:
        x11-dri/libdrm[>=2.4.60][video_drivers:nouveau(-)]
        x11-libs/libpciaccess[>=0.10]
        x11-server/xorg-server[>=1.8]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

