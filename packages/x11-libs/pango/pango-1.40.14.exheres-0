# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008-2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="A library for layout and rendering of text"
HOMEPAGE="http://www.pango.org/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    X [[ description = [ Build Xft backend ] ]]
"

# TODO(?) automagic dependency on libthai (not packaged)
# TODO(compnerd) rename X to truetype to match other packages (e.g. xterm)
DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
        gtk-doc? (
            app-text/docbook-xml-dtd:4.1.2
            dev-doc/gtk-doc[>=1.15]
        )
    build+run:
        dev-libs/glib:2[>=2.33.12]
        media-libs/fontconfig[>=2.10.91]
        media-libs/freetype:2[>=2.1.5]
        x11-libs/cairo[>=1.12.10]
        x11-libs/harfbuzz[>=1.2.3]
        X? (
            x11-libs/libXft[>=2.0.0]
            x11-libs/libXrender
        )
    test:
        fonts/cantarell-fonts
"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--with-cairo' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( 'X xft' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
)

