# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtSpeech"
DESCRIPTION="
The module enables a Qt application to support accessibility features such as
text-to-speech, which is useful for end-users who are visually challenged or
cannot access the application for whatever reason. The most common use case
where text-to-speech comes in handy is when the end-user is driving and cannot
attend the incoming messages on the phone. In such a scenario, the messaging
application can read out the incoming message."

MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-speech/flite
        app-speech/speechd
        sys-sound/alsa-lib
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtmultimedia:${SLOT}[>=${PV}]
"

qtspeech_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtspeech_src_compile() {
    default

    option doc && emake docs
}

qtspeech_src_install() {
    default

    if option doc ; then
        dodoc doc/qtspeech.qch
        docinto html
        dodoc -r doc/qtspeech
    fi

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

